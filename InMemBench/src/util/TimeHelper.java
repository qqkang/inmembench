/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

/**
 * @author chou
 */
public class TimeHelper {

    private long startTime;
    private long endTime;

    public void setStartTime() {
        this.startTime = System.currentTimeMillis();
    }

    public void setEndTime() {
        this.endTime = System.currentTimeMillis();
    }

    public  void getProcessTime(){
        long processTime = endTime - startTime;
        long millSeconds = processTime % 1000;
        processTime /= 1000;
        long seconds = processTime % 60;
        processTime /= 60;
        long minutes = processTime % 60;
        processTime /= 60;
        long hours = processTime;
//       System.out.println("\n该语句执行了:"+hours+"小时"+minutes+"分钟"+seconds+"秒"+millSeconds+"毫秒");  
        System.out.println(hours+"小时"+minutes+"分钟"+seconds+"秒"+millSeconds+"毫秒;\t"); 
//        System.out.println(hours*60*60+minutes*60+seconds+millSeconds*0.001+"秒;\t"); 
    }
}
