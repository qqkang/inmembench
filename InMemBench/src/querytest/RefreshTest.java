package querytest;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import database.DBControl;

import entity.LINEITEM;
import entity.ORDERS;

/**
 * refresh run的测试
 * 
 * @author qqkang
 * 
 */
public class RefreshTest {

	public List<LINEITEM> _lineitems;
	public List<ORDERS> _orders;

	public RefreshTest() {
		_lineitems = new ArrayList<LINEITEM>();
		_orders = new ArrayList<ORDERS>();
	}

	public void readLineitem(String readPath) throws Exception {
		FileReader fr = new FileReader(readPath);
		BufferedReader br = new BufferedReader(fr);
		String readLine;
		while ((readLine = br.readLine()) != null) {
			String[] str = readLine.split("\\|");
			LINEITEM lineitem = new LINEITEM();
			lineitem.setL_ORDERKEY(Integer.parseInt(str[0]));
			lineitem.setL_PARTKEY(Integer.parseInt(str[1].trim()));
			lineitem.setL_SUPPKEY(Integer.parseInt(str[2].trim()));
			lineitem.setL_LINENUMBER(Integer.parseInt(str[3].trim()));
			lineitem.setL_QUANTITY(Double.parseDouble(str[4].trim()));
			lineitem.setL_EXTENDEDPRICE(Double.parseDouble(str[5].trim()));
			lineitem.setL_DISCOUNT(Double.parseDouble(str[6].trim()));
			lineitem.setL_TAX(Double.parseDouble(str[7].trim()));
			lineitem.setL_RETURNFLAG(str[8].trim());
			lineitem.setL_LINESTATUS(str[9].trim());
			lineitem.setL_SHIPDATE(Date.valueOf(str[10].trim()));
			lineitem.setL_COMMITDATE(Date.valueOf(str[11].trim()));
			lineitem.setL_RECEIPTDATE(Date.valueOf(str[12].trim()));
			lineitem.setL_SHIPINSTRUCT(str[13].trim());
			lineitem.setL_SHIPMODE(str[14].trim());
			lineitem.setL_COMMENT(str[15].trim());
			_lineitems.add(lineitem);
		}
		br.close();
		fr.close();
	}

	public void readOrders(String readPath) throws Exception {
		FileReader fr = new FileReader(readPath);
		BufferedReader br = new BufferedReader(fr);
		String readLine;
		while ((readLine = br.readLine()) != null) {
			String[] str = readLine.split("\\|");
			ORDERS orders = new ORDERS();
			// for(int i=0;i<length;i++){
			orders.setO_ORDERKEY(Integer.parseInt(str[0].trim()));
			orders.setO_CUSTKEY(Integer.parseInt(str[1].trim()));
			orders.setO_ORDERSTATUS(str[2].trim());
			orders.setO_TOTALPRICE(Double.parseDouble(str[3].trim()));
			orders.setO_ORDERDATE(Date.valueOf(str[4].trim()));
			orders.setO_ORDERPRIORITY(str[5].trim());
			orders.setO_CLERK(str[6].trim());
			orders.setO_SHIPPRIORITY(Integer.parseInt(str[7]));
			orders.setO_COMMENT(str[8]);
			// }
			_orders.add(orders);
		}
		br.close();
		fr.close();
	}

	public void insert(DBControl dbc, String lineitemPath, String ordersPath) throws Exception {

		File lineitemFile=new File(lineitemPath);
		String insertlineitemSQL="insert into LINEITEM (L_ORDERKEY, L_PARTKEY,L_SUPPKEY," +
				"L_LINENUMBER, " +" L_QUANTITY, L_EXTENDEDPRICE,  L_DISCOUNT, L_TAX, " +
				"L_RETURNFLAG, "+"L_LINESTATUS, L_SHIPDATE, L_COMMITDATE," +
				" L_RECEIPTDATE, L_SHIPINSTRUCT,"+" L_SHIPMODE, L_COMMENT"+
				") values(";
		dbc.executeBatchFromFile(insertlineitemSQL, lineitemFile);
		File ordersFile=new File(ordersPath);
		String insertOrdersSQL="insert into ORDERS (O_ORDERKEY, O_CUSTKEY, O_ORDERSTATUS, " +
				"O_TOTALPRICE, O_ORDERDATE, O_ORDERPRIORITY, O_CLERK, O_SHIPPRIORITY, " +
				"O_COMMENT) values (";
		dbc.executeBatchFromFile(insertOrdersSQL, ordersFile);	
	}

	public void delete(DBControl dbc, String ordersPath) throws Exception {
		File lineitemFile=new File(ordersPath);
		String deleteLineitem="delete from LINEITEM where L_ORDERKEY= ";
		dbc.executeDelFromFile(deleteLineitem, lineitemFile);
		
		File ordersFile=new File(ordersPath);
		String deleteOrders="delete from ORDERS where O_ORDERKEY= ";
		dbc.executeDelFromFile(deleteOrders, ordersFile);
	}

	/**
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub

		DBControl dbc = new DBControl("10.11.1.214", "SAP Hana", "SYSTEM",
				"HanaApp01", "SYSTEM");

		String schema="ZEROFIVE";
		dbc.setSchema(schema);
		RefreshTest rt = new RefreshTest();
		String ordersPath="refresh/orders.tbl.u1";
		String lineitemPath="refresh/lineitem.tbl.u1";
		rt.insert(dbc, lineitemPath, ordersPath);
		rt.delete(dbc, ordersPath);
		
		
		
//		rt.readLineitem("lineitem.tbl.u1");
//		rt.readOrders("orders.tbl.u1");
//		
//		System.out.println(rt._lineitems.size());
//		System.out.println(rt._orders.size());
	}

}
