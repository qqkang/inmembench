/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package querytest;

import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 *
 * @author chou
 */
public class NumberUtils {

    public static boolean isDouble(String str) {
        try {
            Double.parseDouble(str);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public static boolean isInteger(String str) {
        try {
            Integer.parseInt(str);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public static boolean isDate(String str) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        if (str.trim().length() != dateFormat.toPattern().length()) {
            return false;
        }
        dateFormat.setLenient(false);
        try {
            dateFormat.parse(str.trim());
        } catch (ParseException ex) {
            return false;
        }
        return true;
    }
}
