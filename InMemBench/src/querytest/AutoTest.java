package querytest;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

import database.DBControl;

import util.TimeHelper;

public class AutoTest {

	private DBControl _dbc;

	public AutoTest(DBControl dbc) {
		_dbc = dbc;
	}

	/**
	 * 执行查询流
	 * 
	 * @param userNum
	 *            用户量
	 * @throws Exception
	 */
	public void queryStreamTest(int userNum) throws Exception {

		MultiUserTest[] tests = new MultiUserTest[userNum];
		Thread[] thread = new Thread[userNum];
		TimeHelper th = new TimeHelper();
		th.setStartTime();
		for (int i = 0; i < userNum; i++) {
			tests[i] = new MultiUserTest(_dbc);
			thread[i] = new Thread(tests[i]);
			thread[i].start();
		}
		for (int i = 0; i < userNum; i++) {
			thread[i].join();
		}
		th.setEndTime();
		System.out.print("The query total time:\t");
		th.getProcessTime();
	}

	public void refreshTest(double SF) throws Exception{
		RefreshTest refreshTest=new RefreshTest();
		String ordersPath="refresh/"+SF+"/orders.tbl.u1";
		String lineitemPath="refresh/"+SF+"/lineitem.tbl.u1";
		TimeHelper th = new TimeHelper();
		th.setStartTime();
		
		refreshTest.insert(_dbc, lineitemPath, ordersPath);
		refreshTest.delete(_dbc, ordersPath);
		th.setEndTime();
		System.out.print("The refresh total time:\t");
		th.getProcessTime();
	}

	public String getSchema(double SF) {

		if(SF==0.5){
			return "ZEROFIVE";
		}else if(SF==1){
			return "ONE";
		}else if(SF==2){
			return "TWO";
		}else if(SF==5){
			return "FIVE";
		}else if(SF==10){
			return "TEN";
		}
		return "";
	}

	public int getUserNum(double SF) {
		if(SF==0.5){
			return 3;
		}else if(SF==1){
			return 3;
		}else if(SF==2){
			return 3;
		}else if(SF==5){
			return 3;
		}else if(SF==8){
			return 3;
		}else if(SF==10){
			return 3;
		}
		return 0;
	}

	/**
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		DBControl dbc = new DBControl("10.11.1.214", "SAP Hana", "SYSTEM",
				"HanaApp01", "SYSTEM");

		double SF=1;
		AutoTest autoTest=new AutoTest(dbc);
		String schema=autoTest.getSchema(SF);
		dbc.setSchema(schema);
		int userNum=autoTest.getUserNum(SF);
		
		autoTest.queryStreamTest(userNum);
		autoTest.refreshTest(SF);
		
		
		 dbc.close();
	}

} 
