/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package querytest;

import java.io.File;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import database.DBControl;

import util.TimeHelper;

/**
 * @author chou
 */
public class Test {

	String[] dataPath = { "queries/orders.tbl.u1", "queries/lineitem.tbl.u1" };
	String deletePath = "queries/delete.1";

	public Test() throws Exception {
	}

	public List<String> initDefaultPath() {
		List<String> sqlFilePath = new LinkedList();
		for (int i = 1; i <= 8; i++) {
			sqlFilePath.add("query/" + i + ".sql");
		}
		return sqlFilePath;
	}

	public void testQuery(DBControl dbc) throws Exception {
//		System.out.println("========================================");
		List<List<String>> sqlListSet = SQLAnalyzer
				.analyzeList(initDefaultPath());
		TimeHelper th = new TimeHelper();
//		System.out.println("\n");
		int count = 0;
		
		for (int index = 0; index < sqlListSet.size(); index++) {
			List<String> viewSql = sqlListSet.get(index);
			System.out.println("执行第" + (count + 1) + "条语句");
			
			th.setStartTime();
			try {
//				for (int i = 0; i < viewSql.size(); i++) {
//					System.out.print("\n" + viewSql.get(i) + "\n");
//				}
//				System.out.println("\n");
				dbc.executeQueryFromList(viewSql);
			} catch (Exception e) {
				e.printStackTrace(System.out); 
			} finally {
			}
			th.setEndTime();
			th.getProcessTime();
//			System.out.println(count + 1);
			count++;	
		}
		
	}
	
	/**
	 * 
	 * TODO 执行更新流
	 * 
	 * @param dbc
	 * @param times
	 */
	public void testUpdate(DBControl dbc, int times) {

	}

	public static void main(String[] args) throws Exception {

		DBControl dbc = new DBControl("10.11.1.214", "SAP Hana", "SYSTEM",
				"HanaApp01", "SYSTEM");

		String schema="TWO";
		dbc.setSchema(schema);
		Test pt = new Test();
		System.out.println("set schema "+schema);
		 pt.testDefault(dbc);
		 dbc.close();
	}
	public void testDefault(DBControl dbc) throws Exception {
		testQuery(dbc);
	}
	
}
