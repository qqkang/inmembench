package querytest;

import java.util.List;

import database.DBControl;

import util.TimeHelper;

public class MultiUserTest extends Test  implements Runnable{

	DBControl _dbc;
	public MultiUserTest(DBControl dbc) throws Exception {
		_dbc=dbc;
	}
	public void run() {
//		System.out.println("========================================");
		List<List<String>> sqlListSet = SQLAnalyzer
				.analyzeList(initDefaultPath());
//		TimeHelper th = new TimeHelper();
//		System.out.println("\n");
		int count = 0;
		
		for (int index = 0; index < sqlListSet.size(); index++) {
			List<String> viewSql = sqlListSet.get(index);
//			System.out.println("执行第" + (count + 1) + "条语句");
			
//			th.setStartTime();
			try {
//				for (int i = 0; i < viewSql.size(); i++) {
//					System.out.print("\n" + viewSql.get(i) + "\n");
//				}
//				System.out.println("\n");
				_dbc.executeQueryFromList(viewSql);
			} catch (Exception e) {
				e.printStackTrace(System.out); 
			} finally {
			}
//			th.setEndTime();
//			th.getProcessTime();
//			System.out.println(count + 1);
//			count++;	
		}
	}
		
	
	/**
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		DBControl dbc = new DBControl("10.11.1.214", "SAP Hana", "SYSTEM",
				"HanaApp01", "SYSTEM");

		String schema="ZEROFIVE";
		dbc.setSchema(schema);
		int userNum=10;
		
		MultiUserTest[] tests=new MultiUserTest[userNum];
		Thread[] thread=new Thread[userNum];
		
		
		for(int i=0;i<userNum;i++){
			tests[i]=new MultiUserTest(dbc);
			thread[i]=new Thread(tests[i]);
			thread[i].start();
		}
		for(int i=0;i<userNum;i++){
			thread[i].join();
		}
		
		
		 dbc.close();
	}


}
