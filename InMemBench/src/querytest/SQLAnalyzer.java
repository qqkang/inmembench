/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package querytest;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.CharBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.FileChannel.MapMode;
import java.nio.charset.Charset;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;

/**
 * 
 */
public class SQLAnalyzer {

    //从文件读放内容到按分号放到sqlFileList
    public static List<String> analyze(List<String> fileNameList) {
        List<String> sqlList = new ArrayList<String>();
        for (String fileName : fileNameList) {
            File myFile = new File(fileName);
            if (!myFile.exists()) {
                System.err.println("Can't Find " + fileName);
                continue;
            }
            try {
                String tmp = removeComments(myFile);
                String sqls[] = tmp.trim().split(";");
                for(int i=0;i<sqls.length;i++){
                	sqlList.add(sqls[i].trim());
                }
                //sqlList.addAll(Arrays.asList(sqls));
            } catch (FileNotFoundException ex) {
                Logger.getLogger(SQLAnalyzer.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(SQLAnalyzer.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return sqlList;
    }

    public static List<List<String>> analyzeList(List<String> fileNameList) {
        //HashMap<String,List<String>> sqlMap = new HashMap<String,List<String>>();
    	List<List<String>> sqlListSet=new ArrayList<List<String>>();
        for (String fileName : fileNameList) {
            File myFile = new File(fileName);
            if (!myFile.exists()) {
                System.err.println("Can't Find " + fileName);
                continue;
            }
            try {
                String tmp = removeComments(myFile);
                //String sqls[] = tmp.split(";");
                List<String> sqlList = new ArrayList<String>();
                //sqlList.addAll(Arrays.asList(sqls));
                String sqls[] = tmp.trim().split(";");
                for(int i=0;i<sqls.length;i++){
                	sqlList.add(sqls[i].trim());
                }
                String fName = myFile.getName();
                String keyName = fName.substring(0, fName.lastIndexOf("."));
                //sqlMap.put(keyName,sqlList);
                sqlListSet.add(sqlList);
            } catch (FileNotFoundException ex) {
                Logger.getLogger(SQLAnalyzer.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(SQLAnalyzer.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return sqlListSet;
    }
    public static Map<String, List<String>> analyzeSet(List<String> fileNameList) {
        Map<String,List<String>> sqlMap = new HashMap<String,List<String>>();
        for (String fileName : fileNameList) {
            File myFile = new File(fileName);
            if (!myFile.exists()) {
                System.err.println("Can't Find " + fileName);
                continue;
            }
            try {
                String tmp = removeComments(myFile);
                //String sqls[] = tmp.split(";");
                List<String> sqlList = new ArrayList<String>();
                //sqlList.addAll(Arrays.asList(sqls));
                String sqls[] = tmp.trim().split(";");
                for(int i=0;i<sqls.length;i++){
                	sqlList.add(sqls[i].trim());
                }
                String fName = myFile.getName();
                String keyName = fName.substring(0, fName.lastIndexOf("."));
                sqlMap.put(keyName,sqlList);
            } catch (FileNotFoundException ex) {
                Logger.getLogger(SQLAnalyzer.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(SQLAnalyzer.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return sqlMap;
    }
    //Remove All Comments
    public static String removeComments(File file) throws FileNotFoundException, IOException {
        FileChannel channel = new FileInputStream(file).getChannel();
        MappedByteBuffer mb = channel.map(MapMode.READ_ONLY, 0, file.length());
        Charset ascii = Charset.forName("GBK");
        CharBuffer cb = ascii.decode(mb);
        Pattern p = Pattern.compile("(?ms)('(?:''|[^'])*')|--.*?$|/\\*.*?\\*/");
        String presult = p.matcher(cb).replaceAll("$1");
        return presult;
    }
}