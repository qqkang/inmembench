/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

/**
 * 
 * @author chou
 */
public class DBConn {

	private String userName;
	private String password;
	private String dbms;
	private String serverName;
	private String dbName;

	public DBConn(String serverName, String dbms, String userName,
			String password, String dbName) {
		this.serverName = serverName;
		this.dbms = dbms;
		this.userName = userName;
		this.password = password;
		this.dbName = dbName;
	}

	public String getDbms() {
		return this.dbms;
	}

	public Connection getConnection() throws SQLException,
			ClassNotFoundException {
		Connection conn = null;
		Properties connectionProps = new Properties();
		connectionProps.put("user", this.userName);
		connectionProps.put("password", this.password);
		if ("postgresql".equals(this.dbms)) {
			Class.forName("org.postgresql.Driver");
			conn = DriverManager
					.getConnection("jdbc:" + this.dbms + "://"
							+ this.serverName + ":5432" + "/" + dbName,
							connectionProps);
		} else if ("oracle".equals(this.dbms)) {
			Class.forName("oracle.jdbc.driver.OracleDriver");
			conn = DriverManager
					.getConnection("jdbc:" + this.dbms + ":thin:@"
							+ this.serverName + ":1521:" + this.dbName,
							connectionProps);
		} else if ("SAP Hana".equals(this.dbms)) {
			Class.forName("com.sap.db.jdbc.Driver");
			conn = DriverManager.getConnection("jdbc:sap://" + this.serverName
					+ ":30015/" + this.dbName, connectionProps);
		} else if ("timesten".equals(this.dbms)) {
			Class.forName("com.timesten.jdbc.TimesTenDriver");
			conn = DriverManager
					.getConnection("jdbc:timesten:client:DSN=my_cstt");
		} else if ("monetdb".equals(this.dbms)) {
			Class.forName("nl.cwi.monetdb.jdbc.MonetDriver");
			conn = DriverManager.getConnection("jdbc:monetdb://"
					+ this.serverName + ":50000/" + this.dbName,
					connectionProps);
		}
		System.out.println("Connected to database "+this.dbms);
		return conn;
	}

	public static void main(String[] args) throws Exception {
//		DBConn dbconn = new DBConn("localhost", "monetdb", "monetdb",
//				"monetdb", "demo");
		DBConn dbconn = new DBConn("localhost", "timesten", "qqkang",
		"123456", "demo");
		Connection con = dbconn.getConnection();
	}
}