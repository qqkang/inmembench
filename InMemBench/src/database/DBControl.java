/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import java.io.*;
import java.sql.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import querytest.NumberUtils;


/**
 * 
 * @author chou
 */
public class DBControl {

	private DBConn dbConn;
	private Connection conn;

	public DBControl(String serverName, String dbms, String userName,
			String password, String dbName) throws Exception {
		dbConn = new DBConn(serverName, dbms, userName, password, dbName);
		conn = dbConn.getConnection();
		if (conn == null) {
			throw new Exception();
		}
	}

	public void executeCreate(String sql) throws SQLException {
		PreparedStatement ps = conn.prepareStatement(sql);
		ps.executeUpdate();
	}

	public void executeInsert(String sql) throws SQLException {
		PreparedStatement ps = conn.prepareStatement(sql);
		ps.executeUpdate();
	}

	public void executeDelete(String sql) throws Exception {
		PreparedStatement ps = conn.prepareStatement(sql);
		ps.executeUpdate();
	}

	public Boolean executeSelect(String sql) throws SQLException {
		PreparedStatement ps = conn.prepareStatement(sql);
		// return ps.executeQuery();
		return ps.execute();
	}

	public void executeUpdate(String sql) throws SQLException {
		PreparedStatement ps = conn.prepareStatement(sql);
		ps.executeUpdate();
	}

	public void execute(String sql) throws SQLException {
		PreparedStatement ps = conn.prepareStatement(sql);
		ps.execute();
	}

	public String getSourceName() {
		return this.dbConn.getDbms();
	}

	public void setSchema(String schema) throws Exception {
		String sql = "set schema " + schema;
		execute(sql);
	}

	public void close() {
		try {
			this.conn.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void executeBatchFromFile(String sql, File file) throws SQLException {
		try {
			BufferedReader br = new BufferedReader(new FileReader(file));
			conn.setAutoCommit(false);
			String sCurrentLine;
			int count = 0;
			PreparedStatement ps = null;
			while ((sCurrentLine = br.readLine()) != null) {
				String[] element = sCurrentLine.split("\\|");
				if (count == 0) {
					for (int i = 0; i < element.length - 1; i++) {
						sql += "?,";
					}
					sql += "?)";
					ps = conn.prepareStatement(sql);
				}
				for (int i = 0; i < element.length; i++) {
					addCol(ps, i + 1, element[i]);
				}
				ps.addBatch();
				if ((count + 1) % 10000 == 0) {
					ps.executeBatch();
					conn.commit();
				}
				count++;
			}

			ps.executeBatch();
			conn.commit();
		} catch (FileNotFoundException ex) {
			Logger.getLogger(DBCreator.class.getName()).log(Level.SEVERE, null,
					ex);
		} catch (IOException ex) {
			Logger.getLogger(DBControl.class.getName()).log(Level.SEVERE, null,
					ex);
		} catch (SQLException ex) {
			Logger.getLogger(DBControl.class.getName()).log(Level.SEVERE, null,
					ex);
			System.out.println(ex.getNextException());
			conn.rollback();
		} finally {
			conn.setAutoCommit(true);
		}

	}

	public void executeDelFromFile(String sql, File file) throws SQLException {
		try {
			BufferedReader br = new BufferedReader(new FileReader(file));
			conn.setAutoCommit(false);
			String sCurrentLine;
			int count = 0;
			PreparedStatement ps = null;
			while ((sCurrentLine = br.readLine()) != null) {
				String[] element = sCurrentLine.split("\\|", 2);// 分裂两次，下面只取第一列
				if (count == 0) {
					sql += "?";
					ps = conn.prepareStatement(sql);
				}
				addCol(ps, 1, element[0]);
				ps.addBatch();
				if ((count + 1) % 10000 == 0) {
					ps.executeBatch();
					conn.commit();
				}
				count++;
			}

			ps.executeBatch();
			conn.commit();
		} catch (FileNotFoundException ex) {
			Logger.getLogger(DBCreator.class.getName()).log(Level.SEVERE, null,
					ex);
		} catch (IOException ex) {
			Logger.getLogger(DBControl.class.getName()).log(Level.SEVERE, null,
					ex);
		} catch (SQLException ex) {
			Logger.getLogger(DBControl.class.getName()).log(Level.SEVERE, null,
					ex);
			System.out.println(ex.getNextException());
			conn.rollback();
		} finally {
			conn.setAutoCommit(true);
		}

	}

	public void executeQueryFromList(List<String> sqls) {
		for (String item : sqls) {
			try {
				executeSelect(item);
			} catch (SQLException e) {
				e.printStackTrace(System.out); // To change body of catch
												// statement use File | Settings
												// | File Templates.
			}
		}
	}

	public void executeBatchFromList(List<String> sqls) throws SQLException {
		try {
			conn.setAutoCommit(false);
			Statement stmt = conn.createStatement();
			for (String item : sqls) {
				stmt.addBatch(item);
			}
			stmt.executeBatch();
			conn.commit();
		} catch (SQLException e) {
			e.printStackTrace(System.out); // To change body of catch statement
											// use File | Settings | File
											// Templates.
			conn.rollback();
		} finally {
			conn.setAutoCommit(true);
		}
	}

	public void addCol(PreparedStatement ps, int index, String colContent)
			throws SQLException {
		if (NumberUtils.isInteger(colContent)) {
			ps.setInt(index, Integer.parseInt(colContent));
		} else if (NumberUtils.isDouble(colContent)) {
			ps.setDouble(index, Double.parseDouble(colContent));
		} else if (NumberUtils.isDate(colContent)) {
			try {
				SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
				ps.setDate(index, new Date(dateFormat.parse(colContent)
						.getTime()));
			} catch (ParseException ex) {
				Logger.getLogger(DBControl.class.getName()).log(Level.SEVERE,
						null, ex);
			}
		} else {
			ps.setString(index, colContent);
		}
	}

	/**
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		// ResultSet rs = new DBControl("10.11.1.210", "oracle", "BENCHMARK",
		// "BENCH_123", "orcl").executeSelect("select * from customer_10");
		boolean rs = new DBControl("localhost", "postgresql", "postgres",
				"123456", "test")
				.executeSelect("select\n"
						+ "\ts_name,\n"
						+ "\ts_address\n"
						+ "from\n"
						+ "\tsupplier,\n"
						+ "\tnation\n"
						+ "where\n"
						+ "\ts_suppkey in (\n"
						+ "\t\tselect\n"
						+ "\t\t\tps_suppkey\n"
						+ "\t\tfrom\n"
						+ "\t\t\tpartsupp\n"
						+ "\t\twhere\n"
						+ "\t\t\tps_partkey in (\n"
						+ "\t\t\t\tselect\n"
						+ "\t\t\t\t\tp_partkey\n"
						+ "\t\t\t\tfrom\n"
						+ "\t\t\t\t\tpart\n"
						+ "\t\t\t\twhere\n"
						+ "\t\t\t\t\tp_name like 'forest%'\n"
						+ "\t\t\t)\n"
						+ "\t\t\tand ps_availqty > (\n"
						+ "\t\t\t\tselect\n"
						+ "\t\t\t\t\t0.5 * sum(l_quantity)\n"
						+ "\t\t\t\tfrom\n"
						+ "\t\t\t\t\tlineitem\n"
						+ "\t\t\t\twhere\n"
						+ "\t\t\t\t\tl_partkey = ps_partkey\n"
						+ "\t\t\t\t\tand l_suppkey = ps_suppkey\n"
						+ "\t\t\t\t\tand l_shipdate >= date '1994-01-01'\n"
						+ "\t\t\t\t\tand l_shipdate < date '1994-01-01' + interval '1' year\n"
						+ "\t\t\t)\n" + "\t)\n"
						+ "\tand s_nationkey = n_nationkey\n"
						+ "\tand n_name = 'CANADA'\n" + "order by\n"
						+ "\ts_name;");
		// while (rs.next()) {
		// // System.out.println("---");
		// System.out.println(rs.getString(2));
		// }
	}
}
