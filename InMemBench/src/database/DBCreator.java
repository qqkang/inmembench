/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import java.io.File;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import querytest.SQLAnalyzer;


import util.TimeHelper;

/**
 *
 * @author chou
 */
public class DBCreator {

    String path[] = {"src\\tpchTool\\dss.ddl"};
    String dataPath[] = {"dataset\\customer.tbl", "dataset\\lineitem.tbl", "dataset\\nation.tbl",
        "dataset\\orders.tbl", "dataset\\part.tbl", "dataset\\partsupp.tbl", "dataset\\region.tbl",
        "dataset\\supplier.tbl"};

    public void createdb(DBControl dbc) {
        List<String> ddlPath = new LinkedList();
        ddlPath.addAll(Arrays.asList(path));
        List<String> ddl = SQLAnalyzer.analyze(ddlPath);
  
        try {
            int i = 0;
            System.out.println("\n");
            for (String item : ddl) {
                i++;
                System.out.println("执行第" + i + "条语句");
                System.out.println("========================================");
                System.out.println(item);
                dbc.executeCreate(item);
                System.out.println("========================================\n\n");
            }
        } catch (Exception ex) {
            Logger.getLogger(DBCreator.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void loadData(DBControl dbc) {
        for (String tablePath : dataPath) {
            File file = new File(tablePath);
            if (!file.exists()) {
                System.err.println("Can't Find " + tablePath);
                continue;
            }
            String fileName = file.getName();
            String tableName = fileName.substring(0, fileName.lastIndexOf("."));
            String sql = "insert into "+ tableName + " values(";
            System.out.println("========================================");
            double bytes = file.length();
            double kilobytes = (bytes / 1024);
            double megabytes = (kilobytes / 1024);
            double gigabytes = (megabytes / 1024);
            System.out.println("正在载入"+ tableName + "表的数据,文件大小:"+ megabytes + "MB." );
            System.out.println("========================================");
            TimeHelper th = new TimeHelper();
            th.setStartTime();
            try {
                dbc.executeBatchFromFile(sql, file);
            } catch (SQLException ex) {
                Logger.getLogger(DBCreator.class.getName()).log(Level.SEVERE, null, ex);
                ex.getNextException();
            }finally {
                th.setEndTime();
                th.getProcessTime();
            }
        }
        System.out.println("\n ALL DONE.\n");
    }
    
    public void delData(DBControl dbc){
        for (String tablePath : dataPath) {
            File file = new File(tablePath);
            if (!file.exists()) {
                System.err.println("Can't Find " + tablePath);
                continue;
            }
            String fileName = file.getName();
            String tableName = fileName.substring(0, fileName.lastIndexOf("."));
            String sql = "delete from " + tableName;
            System.out.println("========================================");
            System.out.println("清空数据表:" + tableName + ".");
            System.out.println("========================================");
            TimeHelper th = new TimeHelper();
            th.setStartTime();
            try {
                dbc.executeDelete(sql);
            } catch (Exception ex) {
                Logger.getLogger(DBCreator.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                th.setEndTime();
                th.getProcessTime();
            }
        }
        System.out.println("\n ALL DONE.\n");
    }

    public void dropDb(DBControl dbc){
        for (String tablePath : dataPath) {
            File file = new File(tablePath);
            if (!file.exists()) {
                System.err.println("Can't Find " + tablePath);
                continue;
            }
            String fileName = file.getName();
            String tableName = fileName.substring(0, fileName.lastIndexOf("."));
            String sql = "drop table " + tableName; /*+ ";";*/
            System.out.println("========================================");
            System.out.println("删除数据表:" + tableName + ".");
            System.out.println("========================================");
            try {
                dbc.executeDelete(sql);
            } catch (Exception ex) {
                Logger.getLogger(DBCreator.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        System.out.println("\n ALL DONE.\n");
    }

    public static void main(String[] args) {
        try {
        	DBControl dbc=new DBControl("10.11.1.210", "postgresql","postgres", "123456", "benchmark");
        	 DBCreator dbcre = new DBCreator();
             // dbcre.createdb(dbc);
              TimeHelper th = new TimeHelper();
              th.setStartTime();
              dbcre.delData(dbc);
              dbcre.loadData(dbc);

              th.setEndTime();
        } catch (Exception ex) {
            Logger.getLogger(DBCreator.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
