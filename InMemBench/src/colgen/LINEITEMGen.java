package colgen;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;

/**
 * 产生lineitem表的数据，新增200个字段
 * 
 * @author qqkang
 * 
 */
public class LINEITEMGen extends Thread {

	public StringBuffer _recordString;
	public String _readPath;
	public String _writePath;

	
	public Random _random;
	public void initWritePath(String readPath) {
		_readPath = readPath;
		int index = _readPath.lastIndexOf(".");
		_writePath = _readPath.substring(0, index ) + "1"
				+ _readPath.substring(index);
		System.out.println("The data set is being written to file: "+_writePath);
	}

	public int randInt(int min, int max){
		_random=new Random();
		return min+_random.nextInt(max-min);
	}
	public void run() {
		try {
			long time1=System.currentTimeMillis();
			FileReader fr = new FileReader(_readPath);
			BufferedReader br = new BufferedReader(fr);
			FileWriter fw = new FileWriter(_writePath);
			BufferedWriter bw = new BufferedWriter(fw);
			String readLine;
			int num = 0;
			while ((readLine = br.readLine()) != null) {
				_recordString = new StringBuffer();
				_recordString.append(readLine);
				generateCHAR(_recordString);
				generateNUMBER(_recordString);
				generateDATE(_recordString);
				bw.write(_recordString.toString());
				bw.newLine();
				if (num % 5000 == 0) {
					bw.flush();
				}
			}
			bw.flush();
			bw.close();
			fw.close();
			br.close();
			fr.close();
			long time2=System.currentTimeMillis();
			
			long processTime = time2 - time1;
	        long millSeconds = processTime % 1000;
	        processTime /= 1000;
	        long seconds = processTime % 60;
	        processTime /= 60;
	        long minutes = processTime % 60;
	        processTime /= 60;
	        long hours = processTime;
			
			System.out.println("The elapsed time:"+hours+"h"+minutes+"m"+seconds+"s"+millSeconds+"ms");
		} catch (Exception e) {
			System.out.println("The read file error!!!");
		} finally {
			System.exit(1);
		}
	}

	/**
	 * 产生120个字段
	 * 
	 * @param str
	 */
	public void generateCHAR(StringBuffer str) {
		int num=2;
		for (int i = 0; i < num; i++) {// COL1-COL10;
			str.append(Dictionary.CHAR1[randInt(0,26)]);
		}
		for (int i = 0; i < num; i++) {// COL11-COL20;
			str.append(Dictionary.CHAR2[randInt(0,3)]);
		}
		for (int i = 0; i < num; i++) {// COL21-COL30;
			str.append(Dictionary.CHAR3[randInt(0,3)]);
		}
		for (int i = 0; i < num; i++) {// COL31-COL40;
			str.append(Dictionary.CHAR4[randInt(0,3)]);
		}
		for (int i = 0; i < num; i++) {// COL41-COL50;
			str.append(Dictionary.CHAR6[randInt(0,3)]);
		}
		for (int i = 0; i < num; i++) {// COL51-COL60;
			str.append(Dictionary.CHAR6F[randInt(0,3)]);
		}
		for (int i = 0; i < num; i++) {// COL61-COL70;
			str.append(Dictionary.CHAR8F[randInt(0,3)]);
		}
		for (int i = 0; i < num; i++) {// COL71-COL80;
			str.append(Dictionary.CHAR10[randInt(0,3)]);
		}
		for (int i = 0; i < num; i++) {// COL81-COL90;
			str.append(Dictionary.CHAR12[randInt(0,3)]);
		}
		for (int i = 0; i < num; i++) {// CO91-COL100;
			str.append(Dictionary.CHAR10F[randInt(0,3)]);
		}
		for (int i = 0; i < num; i++) {// COL101-COL110;
			str.append(Dictionary.CHAR12F[randInt(0,3)]);
		}
		for (int i = 0; i < num; i++) {// COL111-COL120;
			str.append(Dictionary.CHAR8[randInt(0,3)]);
		}

	}

	/**
	 * 产生40个字段
	 * 
	 * @param str
	 */
	public void generateNUMBER(StringBuffer str) {
		int num=8;
		for(int i=0;i<num;i++){//COL121-COL160
				str.append(Dictionary.DECIMAL[randInt(0,5)]);
		}
	}

	/**
	 * 产生40个字段
	 * 
	 * @param str
	 */
	public void generateDATE(StringBuffer str) {
		int num=8;
		for(int i=0;i<num;i++){//COL161-COL200
				str.append(Dictionary.DATE[randInt(0,7)]);
		}
	}

	/**
	 * @param args
	 * @throws Exception
	 */
	@SuppressWarnings("static-access")
	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub

		LINEITEMGen lineitemGen = new LINEITEMGen();
		lineitemGen.initWritePath("test.txt");
		lineitemGen.start();
	}

	@Override
	public String toString() {
		return "LINEITEMGen [_recordString=" + _recordString + "]";
	}

}
