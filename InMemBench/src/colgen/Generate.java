package colgen;

public class Generate {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		if(args[0].contains("lineitem")){
			LINEITEMGen lineitemGen = new LINEITEMGen();
			lineitemGen.initWritePath(args[0]);
			lineitemGen.start();
		}else if(args[0].contains("order")){
			ORDERSGen ORDERSGen = new ORDERSGen();
			ORDERSGen.initWritePath(args[0]);
			ORDERSGen.start();
		}
	}

}
