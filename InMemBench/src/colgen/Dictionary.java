package colgen;

public class Dictionary {

	static String FSS = "|";

	static String CHAR1[] = {"A|","B|","C|","D|","E|","F|","G|","H|", "I|","J|","K|","L|","M|",
			"N|","O|","P|","Q|", "R|","S|","T|","U|","V|","W|","X|","Y|","Z|"};
	
	static String CHAR2[] = {"EF|","|","DC|"};
	static String CHAR3[] = {"ABC|","|","CJK|"};
	static String CHAR4[] = {"UDJK|","|","UIIU|"};
	static String CHAR6[] = {"123456|","|","TYUIUY|"};
	static String CHAR6F[] = {"123456|","YUFED|","TYUIUY|"};
	static String CHAR8F[] = {"YUFEDFS|","IUGTRED|","ITYUIUY|"};
	static String CHAR10[] = {"00FGHKRT|","|","YUIKD0IO|"};
	static String CHAR12[] = {"ABCDEFGHKRT|","|","YUIKDFUIIOIJ|"};
	static String CHAR10F[] = {"ABCDEFGHIJ|","9876543210|","WE789UIJH|"};
	static String CHAR12F[] = {"100000000001|","100000000002|","100000000003|"};
	static String CHAR8[] = {"|","IUGTRED|","ITYUIUY|"};
	static String DATE[] = {"1992-02-23|","|","1993-01-01|","|","1995-04-01|","1996-12-02|","1997-08-28|"};//7个值，一个空
	static String DECIMAL[] = {"343225321.23|","878986623.23|","9123212.12|","|","|"};//5个值，二个空
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.out.println(Dictionary.DATE[2]);

	}

}
