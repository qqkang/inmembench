package entity;

import java.sql.Date;

public class LINEITEM {

//	L_ORDERKEY    INTEGER NOT NULL,
//    L_PARTKEY     INTEGER NOT NULL,
//    L_SUPPKEY     INTEGER NOT NULL,
//    L_LINENUMBER  INTEGER NOT NULL,
//    L_QUANTITY    DECIMAL(15,2),
//    L_EXTENDEDPRICE  DECIMAL(15,2),                  
//    L_DISCOUNT    DECIMAL(15,2),
//    L_TAX         DECIMAL(15,2),
//    L_RETURNFLAG  CHAR(1),
//    L_LINESTATUS  CHAR(1),
//    L_SHIPDATE    DATE,
//    L_COMMITDATE  DATE,
//    L_RECEIPTDATE DATE,
//    L_SHIPINSTRUCT CHAR(25),
//    L_SHIPMODE     CHAR(10),
//    L_COMMENT      VARCHAR(44),
	public int L_ORDERKEY;
	public int L_PARTKEY;
	public int L_SUPPKEY;
	public int L_LINENUMBER;
	public double L_QUANTITY;
	public double L_EXTENDEDPRICE;
	public double L_DISCOUNT;
	public double L_TAX;
	public String L_RETURNFLAG;
	public String L_LINESTATUS;
	public Date L_SHIPDATE;//若要插入到数据库并且相应的字段为Date类型
	public Date L_COMMITDATE;//若要插入到数据库并且相应的字段为Date类型
	public Date L_RECEIPTDATE;//若要插入到数据库并且相应的字段为Date类型
	public String L_SHIPINSTRUCT;
	public String L_SHIPMODE;
	public String L_COMMENT;
	
	public int getL_ORDERKEY() {
		return L_ORDERKEY;
	}

	public void setL_ORDERKEY(int l_ORDERKEY) {
		L_ORDERKEY = l_ORDERKEY;
	}

	public int getL_PARTKEY() {
		return L_PARTKEY;
	}

	public void setL_PARTKEY(int l_PARTKEY) {
		L_PARTKEY = l_PARTKEY;
	}

	public int getL_SUPPKEY() {
		return L_SUPPKEY;
	}

	public void setL_SUPPKEY(int l_SUPPKEY) {
		L_SUPPKEY = l_SUPPKEY;
	}

	public int getL_LINENUMBER() {
		return L_LINENUMBER;
	}

	public void setL_LINENUMBER(int l_LINENUMBER) {
		L_LINENUMBER = l_LINENUMBER;
	}

	public double getL_QUANTITY() {
		return L_QUANTITY;
	}

	public void setL_QUANTITY(double l_QUANTITY) {
		L_QUANTITY = l_QUANTITY;
	}

	public double getL_EXTENDEDPRICE() {
		return L_EXTENDEDPRICE;
	}

	public void setL_EXTENDEDPRICE(double l_EXTENDEDPRICE) {
		L_EXTENDEDPRICE = l_EXTENDEDPRICE;
	}

	public double getL_DISCOUNT() {
		return L_DISCOUNT;
	}

	public void setL_DISCOUNT(double l_DISCOUNT) {
		L_DISCOUNT = l_DISCOUNT;
	}

	public double getL_TAX() {
		return L_TAX;
	}

	public void setL_TAX(double l_TAX) {
		L_TAX = l_TAX;
	}

	public String getL_RETURNFLAG() {
		return L_RETURNFLAG;
	}

	public void setL_RETURNFLAG(String l_RETURNFLAG) {
		L_RETURNFLAG = l_RETURNFLAG;
	}

	public String getL_LINESTATUS() {
		return L_LINESTATUS;
	}

	public void setL_LINESTATUS(String l_LINESTATUS) {
		L_LINESTATUS = l_LINESTATUS;
	}

	public Date getL_SHIPDATE() {
		return L_SHIPDATE;
	}

	public void setL_SHIPDATE(Date l_SHIPDATE) {
		L_SHIPDATE = l_SHIPDATE;
	}

	public Date getL_COMMITDATE() {
		return L_COMMITDATE;
	}

	public void setL_COMMITDATE(Date l_COMMITDATE) {
		L_COMMITDATE = l_COMMITDATE;
	}

	public Date getL_RECEIPTDATE() {
		return L_RECEIPTDATE;
	}

	public void setL_RECEIPTDATE(Date l_RECEIPTDATE) {
		L_RECEIPTDATE = l_RECEIPTDATE;
	}

	public String getL_SHIPINSTRUCT() {
		return L_SHIPINSTRUCT;
	}

	public void setL_SHIPINSTRUCT(String l_SHIPINSTRUCT) {
		L_SHIPINSTRUCT = l_SHIPINSTRUCT;
	}

	public String getL_SHIPMODE() {
		return L_SHIPMODE;
	}

	public void setL_SHIPMODE(String l_SHIPMODE) {
		L_SHIPMODE = l_SHIPMODE;
	}

	public String getL_COMMENT() {
		return L_COMMENT;
	}

	public void setL_COMMENT(String l_COMMENT) {
		L_COMMENT = l_COMMENT;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
