package entity;

import java.sql.Date;

public class ORDERS {
	// O_ORDERKEY INTEGER NOT NULL,
	// O_CUSTKEY INTEGER NOT NULL,
	// O_ORDERSTATUS CHAR(1),
	// O_TOTALPRICE DECIMAL(15,2),
	// O_ORDERDATE DATE,
	// O_ORDERPRIORITY CHAR(15),
	// O_CLERK CHAR(15),
	// O_SHIPPRIORITY INTEGER,
	// O_COMMENT VARCHAR(79),

	public int O_ORDERKEY;
	public int O_CUSTKEY;
	public String O_ORDERSTATUS;
	public double O_TOTALPRICE;
	public Date O_ORDERDATE;
	public String O_ORDERPRIORITY;
	public String O_CLERK;
	public int O_SHIPPRIORITY;
	public String O_COMMENT;

	public int getO_ORDERKEY() {
		return O_ORDERKEY;
	}

	public void setO_ORDERKEY(int o_ORDERKEY) {
		O_ORDERKEY = o_ORDERKEY;
	}

	public int getO_CUSTKEY() {
		return O_CUSTKEY;
	}

	public void setO_CUSTKEY(int o_CUSTKEY) {
		O_CUSTKEY = o_CUSTKEY;
	}

	public String getO_ORDERSTATUS() {
		return O_ORDERSTATUS;
	}

	public void setO_ORDERSTATUS(String o_ORDERSTATUS) {
		O_ORDERSTATUS = o_ORDERSTATUS;
	}

	public double getO_TOTALPRICE() {
		return O_TOTALPRICE;
	}

	public void setO_TOTALPRICE(double o_TOTALPRICE) {
		O_TOTALPRICE = o_TOTALPRICE;
	}

	public Date getO_ORDERDATE() {
		return O_ORDERDATE;
	}

	public void setO_ORDERDATE(Date o_ORDERDATE) {
		O_ORDERDATE = o_ORDERDATE;
	}

	public String getO_ORDERPRIORITY() {
		return O_ORDERPRIORITY;
	}

	public void setO_ORDERPRIORITY(String o_ORDERPRIORITY) {
		O_ORDERPRIORITY = o_ORDERPRIORITY;
	}

	public String getO_CLERK() {
		return O_CLERK;
	}

	public void setO_CLERK(String o_CLERK) {
		O_CLERK = o_CLERK;
	}

	public int getO_SHIPPRIORITY() {
		return O_SHIPPRIORITY;
	}

	public void setO_SHIPPRIORITY(int o_SHIPPRIORITY) {
		O_SHIPPRIORITY = o_SHIPPRIORITY;
	}

	public String getO_COMMENT() {
		return O_COMMENT;
	}

	public void setO_COMMENT(String o_COMMENT) {
		O_COMMENT = o_COMMENT;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
